<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Task;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Task::class;
    use WithFaker;
    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'phone' => $this->faker->regexify('^[0-9]{10}'),
        ];
    }
}
