@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        {{-- <div class="card-header">Dashboard</div> --}}

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Tasks</h4>
                        </div>

                        <div class="card-body">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th scope ="col">ID</th>
                                <th scope ="col">Name</th>
                                <th scope ="col">Content</th>
                                <th scope ="col">Phone</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$task->id }}</th>
                                    <td>{{$task->name }}</td>
                                    <td>{{$task->content }}</td>
                                    <td>{{$task->phone}}</td>
                                </tr>
                            </tbody>
                          </table>
                         
                        </div>
                    </div>
                </div>
            </div>    
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
@endsection
