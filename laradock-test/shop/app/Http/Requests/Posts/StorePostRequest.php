<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationData;
use Illuminate\Validation\ValidationException;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'phone' => 'required|string|regex:/^[0-9]{10}$/',
        ];
    }
    
    
    protected function failedValidation(Validator $validator)
    {
        
        $errors = $validator->errors();
    
        
        if ($errors->has('phone')) {
            
            $errors->add('phone', 'Phone must not be empty and has 10 number characters');
        }
    
        $response = Response([
            'errors' => $errors,
            'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    
        throw new ValidationException($validator, $response);
    }
    
}
