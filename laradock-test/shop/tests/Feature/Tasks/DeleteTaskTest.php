<?php

namespace Tests\Feature\Tasks;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;

class DeleteTaskTest extends TestCase
{

    public function deleteTaskRoute($id)
    {
        return route('tasks.destroy', ['id' => $id]);
    }
    /**
     @test
     */
    public function unauthenticated_user_can_not_delete_task(): void
    {
        $task = Task::factory()->create();
        $response = $this->put($this->deleteTaskRoute($task->id),$task->toArray());
        $response -> assertRedirect('/login');
    }

    /**
     @test
     */
    public function authenticated_user_can_delete_task(): void
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->delete($this->deleteTaskRoute($task->id));
        $response -> assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('tasks', $task->toArray());

    }
}
