<?php

namespace Tests\Feature\Task;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Task;
use Illuminate\Http\Response;

class getListTaskTest extends TestCase
{
    public function getListTaskRoute()
    {
        return route('tasks.index');
    }
    /**
     @test
     */
    public function user_can_get_all_task(): void
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
        
    }
}
