<?php

namespace Tests\Feature\Tasks;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Task;
use Illuminate\Http\Response;
use App\Models\User;

class GetTaskTest extends TestCase
{
    /**
     @test
     */
    public function user_can_get_task_if_task_exists(): void
    { 
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.show', $task->id));

        $response->assertStatus(Response::HTTP_OK);
    }
}
