<?php

namespace Tests\Feature\Tasks;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Task;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use App\Models\User;

class UpdateTaskTest extends TestCase
{

    public function editTaskRoute($id)
    {
        return route('tasks.edit', ['id' => $id]);
    }

    public function updateTaskRoute($id)
    {
        return route('tasks.update', ['id' => $id]);
    }
    /**
     @test
     */
    public function unauthenticated_user_can_not_see_edit_task_form_view(): void
    { 
        $task = Task::factory()->create();
        // $response = $this->get(route('tasks.edit', ['id' => $task->id]));
        $response = $this->get($this->editTaskRoute($task->id));
        $response->assertRedirect('/login');
    }

    /**
     @test
     */    
    public function authenticated_user_can_update_task_if_task_exists_and_data_is_valid()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'phone' => $this->faker->regexify('^[0-9]{10}'),
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        
        $this->assertDatabaseHas('tasks',[
            'name' => $dataUpdate['name'],
            'content' => $dataUpdate['content'],
            'phone' => $dataUpdate['phone'],
        ]);
    }

    /**
     @test
     */  
    public function authenticated_user_can_not_update_task_if_task_not_exists_and_data_is_valid()
    {
        $this->actingAs(User::factory()->make());
        $task_id = -1;
        $dataUpdate = [
            'name' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'phone' => $this->faker->regexify('^[0-9]{10}'),
        ];
        $response = $this->put($this->updateTaskRoute($task_id), $dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
    /**
     @test
     */
    public function authenticated_user_can_not_update_task_if_task_exists_and_name_field_is_null()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => null,
            'content' => $this->faker->paragraph,
            'phone' => $this->faker->regexify('^[0-9]{10}'),
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);

        $response->assertSessionHasErrors('name');
        
    }

    /**
     @test
     */
    public function authenticated_user_can_not_update_task_if_task_exists_and_content_field_is_null()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->sentence,
            'content' => null,
            'phone' => $this->faker->regexify('^[0-9]{10}'),
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);

        $response->assertSessionHasErrors('content');
        
    }

    /**
     @test
     */
    public function authenticated_user_can_not_update_task_if_task_exists_and_phone_field_is_null()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'phone' => '12345'
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);
        $response->assertSessionHasErrors('phone');
        
    }

    /**
     @test
     */

     public function authenticated_user_can_not_update_task_if_task_exists_and_data_is_null ()
     {
        $this->actingAs(User::factory()->make());
         $task = Task::factory()->create();
         $dataUpdate = [
             'name' => null,
             'content' => null,
             'phone' => null,
         ];
         $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);

         $response->assertSessionHasErrors('name');
         $response->assertSessionHasErrors('content');
         $response->assertSessionHasErrors('phone');
         
         
     }

    /**
     @test
     */
   public function authenticated_user_can_see_edit_task_form_view () 
   {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get($this->editTaskRoute($task->id));
        $response->assertViewIs('tasks.edit');
   }
   
  
}


